#!/usr/bin/env python


#############################################################################
##
## Copyright (C) 2012 Riverbank Computing Limited.
## Copyright (C) 2012 Digia Plc
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################


# This is only needed for Python v2 but is harmless for Python v3.
import sip

sip.setapi('QString', 2)

from PyQt4 import QtCore, QtGui
from os import path
from sys import argv, exit
from xml.dom import minidom
from Modules import sentenceBuilder
from Modules import sentenceElement

import editor_rc


class Test(QtGui.QWidget):
  def __init__( self, labelText, parent=None):
      super(Test, self).__init__(parent)

      self.completingTextEdit = TextEdit()
      self.completer = QtGui.QCompleter(self)

      sb = sentenceBuilder.sentenceBuilder('XMLConverter/sentencesStructure.XML')
      self.completingTextEdit.setSentenceBuilder(sb)
      output = sb.getNext('')
      words = []
      for element in output[1]:
          words.append(element.getValue())
      self.completer.setModel(QtGui.QStringListModel(words, self.completer))
      self.completer.setModelSorting(QtGui.QCompleter.CaseInsensitivelySortedModel)
      self.completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
      self.completer.setWrapAround(False)
      self.completingTextEdit.setCompleter(self.completer)

      self.label = QtGui.QLabel(labelText)
	
      layout = QtGui.QVBoxLayout()
      layout.addWidget(self.label)
      layout.addWidget(self.completingTextEdit)
      self.setLayout(layout)

class TextEdit(QtGui.QTextEdit):
    def __init__(self, parent=None):
        super(TextEdit, self).__init__(parent)

        self._completer = None
        self._sentenceBuilder = None

    def setCompleter(self, c):
        if self._completer is not None:
            self._completer.activated.disconnect()

        self._completer = c

        c.setWidget(self)
        c.setCompletionMode(QtGui.QCompleter.PopupCompletion)
        c.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        c.activated.connect(self.insertCompletion)

    def completer(self):
        return self._completer

    def setSentenceBuilder(self, s):
        self._sentenceBuilder = s

    def sentenceBuilder(self):
        return self._sentenceBuilder

    def textUnderCursor(self):
        tc = self.textCursor()
        tc.select(QtGui.QTextCursor.WordUnderCursor)
        return tc.selectedText()

    def lineUnderCursor(self):
        tc = self.textCursor()
        tc.select(QtGui.QTextCursor.LineUnderCursor)
        return tc.selectedText()

    def insertCompletion(self, completion):
        if self._completer.widget() is not self:
            return

        if self.textUnderCursor() == completion:
            return
        
        tc = self.textCursor()
        extra = len(completion) - len(self._completer.completionPrefix())
        tc.movePosition(QtGui.QTextCursor.EndOfWord)
        tc.insertText(completion[-extra:])
        self.setTextCursor(tc)

    def focusInEvent(self, e):
        if self._completer is not None:
            self._completer.setWidget(self)

        super(TextEdit, self).focusInEvent(e)

    def formatInput(self):
        textInTheBox = self.toPlainText()
        #
        #here goes parser code
        #
        tekstWyjsciowy = ""
        licznik = 0

        #sb = sentenceBuilder.sentenceBuilder('XMLConverter/sentencesStructure.XML')
        output = self._sentenceBuilder.getNext(self.lineUnderCursor())
        #print self.lineUnderCursor()
        #print output[2]

        oldCursor = self.textCursor().position()
        myCursor = self.textCursor()

        #self.setHtml('<font color=red>'+textInTheBox+'</font>')

        myCursor.select(QtGui.QTextCursor.LineUnderCursor)
        myCursor.removeSelectedText()
        self.insertHtml(output[2])

        myCursor.setPosition(oldCursor)
        self.setTextCursor(myCursor)

    def keyReleaseEvent(self, e):
        #print e.key()
        if e.key() != 16777220 and e.key() != 32:
            self.formatInput()
        #output = self._sentenceBuilder.getNext(self.lineUnderCursor())
        #tc = self.textCursor()
        #tc.select(QtGui.QTextCursor.LineUnderCursor)
        #tc.removeSelectedText()
        #tc.insertHtml(output[2])
        #self.formatInput()
        #return

    def keyPressEvent(self, e):
        if self._completer is not None and self._completer.popup().isVisible():
            if e.key() in (QtCore.Qt.Key_Enter, QtCore.Qt.Key_Return, QtCore.Qt.Key_Escape, QtCore.Qt.Key_Tab, QtCore.Qt.Key_Backtab):
                e.ignore()
                return

        modifiers = e.modifiers()
        isShortcut = (modifiers == QtCore.Qt.ControlModifier and e.key() == QtCore.Qt.Key_Space)
        if self._completer is None or not isShortcut:
            super(TextEdit, self).keyPressEvent(e)

        output = self._sentenceBuilder.getNext(self.lineUnderCursor())
        words = []
        for element in output[1]:
            words.append(element.getValue())
        self._completer.setModel(QtGui.QStringListModel(words, self._completer))

        completionPrefix = self.textUnderCursor()

        if completionPrefix != self._completer.completionPrefix():
            self._completer.setCompletionPrefix(completionPrefix)
            self._completer.popup().setCurrentIndex(self._completer.completionModel().index(0, 0))

        cr = self.cursorRect()
        cr.setWidth(self._completer.popup().sizeHintForColumn(0) + self._completer.popup().verticalScrollBar().sizeHint().width())
        self._completer.complete(cr)


class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

	 # main button
        self.addButton = QtGui.QPushButton('button to add other widgets')
        self.addButton.clicked.connect(self.addWidget)

        # scroll area widget contents - layout
        self.scrollLayout = QtGui.QFormLayout()

        # scroll area widget contents
        self.scrollWidget = QtGui.QWidget()
        self.scrollWidget.setLayout(self.scrollLayout)

        # scroll area
        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollWidget)

        # main layout
        self.mainLayout = QtGui.QVBoxLayout()

        # add all main to the main vLayout
        self.mainLayout.addWidget(self.addButton)
        self.mainLayout.addWidget(self.scrollArea)

        # central widget
        self.centralWidget = QtGui.QWidget()
        self.centralWidget.setLayout(self.mainLayout)

        # set central widget
        self.setCentralWidget(self.centralWidget)
	
    
        self.createMenu()
	

        

       # self.setCentralWidget(self.completingTextEdit)
        self.resize(500, 300)
        self.setWindowTitle("TCEdit")

    def addWidget(self):
        self.scrollLayout.addRow(Test('testowy tekst'))

    def getTextAttribute(self, node):
        if node.attributes:
            for i in range(node.attributes.length):
                if node.attributes.item(i).name == 'value':
                    return node.attributes.item(i).value

    def readNodes(self, inputNodes, outputList):
        for element in inputNodes:
            if element.attributes:
                attr = self.getTextAttribute(element)
                outputList.insert(len(outputList), [self.getTextAttribute(element)])
            if element.childNodes:
                self.readNodes(element.childNodes, outputList[len(outputList)-1])

    def findChild(self, myList, childName):
        for idx, elem in enumerate(myList):
            if childName in elem:
                return idx
        return -1

    #returns 'children'
    def goDeeperForCompletion(self, myList):
        toReturn = []
        self.goDeeperForCompletionRecursive(myList[1:], toReturn)
        return toReturn

    def goDeeperForCompletionRecursive(self, myList, toReturn):
        for elem in myList:
            if elem[0][0] == '[':
                toReturn = (self.goDeeperForCompletionRecursive(elem[1:], toReturn))
            else:
                for idx, elem in enumerate(myList):
                    toReturn.append(elem)
        return toReturn

    def getCurrentOptions(self, myList):
        toReturn = []
        for elem in myList:
                toReturn.append(elem[0])
        return toReturn

    def get_data(self, model):
        #model.setStringList(["completion", "data", "goes", "here"]) #works
        self.currentOptions = self.getCurrentOptions(self.completionInformation)
        #self.currentOptions = ['foo', 'bar', 'chicken'] #doesn't work
        model.setStringList(self.currentOptions)
        print self.getCurrentOptions(self.completionInformation)

    def createMenu(self):
        exitAction = QtGui.QAction("Exit", self)

        exitAction.triggered.connect(QtGui.qApp.quit)

        fileMenu = self.menuBar().addMenu("File")
        fileMenu.addAction(exitAction)

    def modelFromFile(self, fileName):
        f = QtCore.QFile(fileName)
        if not f.open(QtCore.QFile.ReadOnly):
            return QtGui.QStringListModel(self.completer)

        QtGui.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))

        words = []
        while not f.atEnd():
            line = f.readLine().trimmed()
            if line.length() != 0:
                try:
                    line = str(line, encoding='ascii')
                except TypeError:
                    line = str(line)

                words.append(line)

        QtGui.QApplication.restoreOverrideCursor()

        return QtGui.QStringListModel(words, self.completer)

if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
