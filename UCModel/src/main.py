# -*- coding: utf-8 -*-

from model import *
from writer import text
from writer import xml

project = Project()
project.name = "Example"
project.language = "en"
project.version = "0.1"


spec = UCSpec()
project.ucspec = spec

gaol_busi = GoalLevel("Biznesowy")
gaol_user = GoalLevel(u"Użytkownika")

prio_high = Priority("Wysoki")
prio_midd = Priority(u"Średni")
prio_low  = Priority("Niski")

akt_zlec = Actor(u"Zlecający",
				 "AKT_ZLEC",
				 u"Zlecający to pracownik P (obecnie P zatrudnia około 3000 osób), który przydziela zadania organizacyjne innym pracownikom PP.")

akt_wyko = Actor("Wykonawca",
				 "AKT_WYK",
				 u"Wykonawca to pracownik P, który otrzymuje zadania organizacyjne od innych, uprawnionych do tego, pracowników PP.")
akt_wyko.properties = { 'przypadek': {
					u'mianownik':   { 'liczba': { 'pojedyncza': u'wykonawca', 'mnoga': u'wykonawcy' } },
					u'dopełniacz':  { 'liczba': { 'pojedyncza': u'wykonawcy', 'mnoga': u'wykonawców' } },
					u'celownik':    { 'liczba': { 'pojedyncza': u'wykonawcy', 'mnoga': u'wykonawcom' } },
					u'biernik':     { 'liczba': { 'pojedyncza': u'wykonawcę', 'mnoga': u'wykonawców' } },
					u'narzędnik':   { 'liczba': { 'pojedyncza': u'wykonawcą', 'mnoga': u'wykonawcami' } },
					u'miejscownik': { 'liczba': { 'pojedyncza': u'wykonawcy', 'mnoga': u'wykonawcach' } },
					u'wołacz':      { 'liczba': { 'pojedyncza': u'wykonawco', 'mnoga': u'wykonawcy' } }
				}}

akt_data = Actor("Administrator danych",
				 "AKT_DATA_ADM",
				 u"Administrator danych to wyznaczony pracownik P, którego zadaniem jest wprowadzanie danych dotyczących innych pracowników do Systemu RPO.")
akt_tech = Actor("Administrator techniczny",
				 "AKT_TECH_ADM",
				 u"Administrator techniczny to wyznaczony pracownik P, który jest odpowiedzialny za archiwizację bazy danych, konfigurowanie systemu oraz generowanie raportów.")
akt_prze = Actor(u"Przełożony wykonawcy",
				 "AKT_PRZE_WYK",
				 u"Przełożony wykonawcy to pracownik P, który jest szefem wykonawcy.")

bo_zad = BusinessObject("Zadanie", "BO_ZAD")
bo_rai = BusinessObject("Raport indywidualny", "BO_RAI")
bo_rag = BusinessObject("Raport grupowy", "BO_RAG")
bo_tab = BusinessObject("Tablica", "BO_TAB")
bo_lis = BusinessObject(u"Lista wykonawców", "BO_LIS")

br_1 = BusinessRule("Reg 1")
br_1.description = [u"Zadanie może być..."]
br_2 = BusinessRule("Reg 2")
br_2.description = [u"Zadanie może mieć..."]
br_3 = BusinessRule("Reg 3")
br_3.description = [u"Wykonawca zadanie w..."]
br_4 = BusinessRule("Reg 4")
br_4.description = [u"Zadanie może znaźć..."]

project.actors.append(akt_zlec)
project.actors.append(akt_wyko)
project.actors.append(akt_data)
project.actors.append(akt_tech)
project.actors.append(akt_prze)
spec.priorities.append(prio_high)
spec.priorities.append(prio_midd)
spec.priorities.append(prio_low)
spec.goal_levels.append(gaol_busi)
spec.goal_levels.append(gaol_user)
project.business_objects.append(bo_zad)
project.business_objects.append(bo_rai)
project.business_objects.append(bo_rag)
project.business_objects.append(bo_tab)
project.business_objects.append(bo_lis)
project.business_rules.append(br_1)
project.business_rules.append(br_2)
project.business_rules.append(br_3)
project.business_rules.append(br_4)

bc_01 = UseCase([TextItem(u"Realizacja"),
				 bo_zad.get_ref(),
				 TextItem(u"organizacyjnego")])
bc_01.identifier = "BC_001"
bc_01.goal_level = gaol_busi.get_ref()
bc_01.priority = prio_high.get_ref()
bc_01.main_actors.append(akt_zlec.get_ref())
bc_01.main_actors.append(akt_wyko.get_ref())

bc_01.triggers.append(Trigger([
		TextItem(u"to jest trigger"),
		bo_zad.get_ref(),
		akt_wyko.get_ref({'przypadek': u'dopełniacz', 'liczba': u'pojedyńcza'})]))
bc_01.preconditions.append(PreCondition([
		TextItem(u"to jest precondition"),
		bo_zad.get_ref(),
		akt_wyko.get_ref({'przypadek': u'dopełniacz', 'liczba': u'pojedyńcza'})]))
bc_01.postconditions.append(PostCondition([
		TextItem(u"to jest postcondition"),
		bo_zad.get_ref(),
		akt_wyko.get_ref({'przypadek': u'dopełniacz', 'liczba': u'pojedyńcza'})]))

bc_01.scenario = Scenario()
st_01 = Step([akt_zlec.get_ref(),
			  TextItem(u"pragnie przydzielić"),
			  bo_zad.get_ref(),
			  TextItem("konkretnemu"),
			  akt_wyko.get_ref({'przypadek': u'dopełniacz', 'liczba': u'pojedyńcza'}),
			  TextItem("."),
			  akt_zlec.get_ref(),
			  TextItem("definiuje"),
			  bo_zad.get_ref(),
			  TextItem(".")])
bc_01.scenario.items.append(st_01)
st_02 = Step([akt_wyko.get_ref(),
			  TextItem(u"akceptuje przydział do"),
			  bo_zad.get_ref(),
			  TextItem(".")])
bc_01.scenario.items.append(st_02)
st_03 = Step([akt_wyko.get_ref(),
			  TextItem("wykonuje"),
			  bo_zad.get_ref(),
			  TextItem(".")])
bc_01.scenario.items.append(st_03)
bc_01.scenario.items.append(Step([akt_wyko.get_ref(),
								  TextItem(u"zgłasza wykonanie"),
								  bo_zad.get_ref(),
								  TextItem(".")]))
bc_01.scenario.items.append(Step([akt_zlec.get_ref(),
								  TextItem("rozlicza"),
								  bo_zad.get_ref(),
								  TextItem("."),
								  EoUCCommand()]))

ev_01 = AlternationEvent([akt_zlec.get_ref(),
						  TextItem(u"pragnie umieścić"),
						  bo_zad.get_ref(),
						  TextItem(" na"),
						  bo_tab.get_ref(),
						  TextItem(".")])
ev_01.scenario = Scenario()
ev_01.scenario.items.append(Step([akt_zlec.get_ref(),
								  TextItem("umieszcza "),
								  bo_zad.get_ref(),
								  TextItem(" na"),
								  bo_tab.get_ref(),
								  TextItem(".")]))
ev_01.scenario.items.append(Step([akt_wyko.get_ref(),
								  TextItem(u"zgłasza się do wykonania"),
								  bo_zad.get_ref(),
								  TextItem(".")]))
ev_01.scenario.items.append(Step([akt_zlec.get_ref(),
								  TextItem("akceptuje"),
								  akt_wyko.get_ref(),
								  TextItem(".")]))
ev_01.scenario.items.append(Step([TextItem(u"Przejdź do kroku"),
								  GoToCommand(st_03),
								  TextItem(".")]))
st_01.events.append(ev_01)

ev_02 = AlternationEvent([akt_wyko.get_ref(),
						  TextItem(u"pragnie przypisać sobie"),
						  bo_zad.get_ref(),
						  TextItem(u"przydzielone mu “poza systemem” przez"),
						  akt_zlec.get_ref(),
						  TextItem(".")])
ev_02.scenario = Scenario()
ev_02.scenario.items.append(Step([akt_wyko.get_ref(),
								 TextItem("definiuje"),
								 bo_zad.get_ref(),
								 TextItem(".")]))
ev_02.scenario.items.append(Step([akt_zlec.get_ref(),
							  TextItem("potwierdza"),
						      bo_zad.get_ref(),
						      TextItem(".")]))
ev_02.scenario.items.append(Step([TextItem(u"Przejdź do kroku"),
							  GoToCommand(st_03),
							  TextItem(".")]))
st_01.events.append(ev_02)

ev_03 = AlternationEvent([akt_wyko.get_ref(),
						  TextItem(u"odrzuca przydział do"),
						  bo_zad.get_ref(),
						  TextItem(".")])
ev_03.scenario = Scenario()
ev_03.scenario.items.append(Step([akt_wyko.get_ref(),
							  TextItem(u"odrzuca przydział do"),
							  bo_zad.get_ref(),
							  TextItem(".")]))
ev_03.scenario.items.append(Step([akt_zlec.get_ref(),
							  TextItem(u"zostaje poinformowany o odrzuceniu przydziału do"),
							  bo_zad.get_ref(),
							  TextItem("przez wskazanego"),
							  akt_wyko.get_ref(),
							  TextItem(".")]))
ev_03.scenario.items.append(Step([TextItem(u"Koniec przypadku użycia."),
							  EoUCCommand()]))
st_02.events.append(ev_03)

spec.usecases.append(bc_01)


xml.attach();
retval = project.to_xml();
print retval
open("tmp.xml", "w").write(retval)

print retval,

xml.detach()
