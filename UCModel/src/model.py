'''
Created on Dec 14, 2012

@author: Bartosz Alchimowicz
'''

##############
# Reference
######

class Referencable(object):
	def __init__(self):
		self.referenced = False					# boolean

	def get_ref(self, properties = None):
		self.referenced = True
		return Reference(self, properties)

##############
# Dependable
######

class Configuration(object):
	def __init__(self):
		pass

class GoalLevel(Configuration, Referencable):
	def __init__(self, name):
		Referencable.__init__(self)

		self.name = name						# str

class Priority(Configuration, Referencable):
	def __init__(self, name):
		Referencable.__init__(self)

		self.name = name						# str

##############
# Independable
######

class Project(object):
	def __init__(self):
		self.name = None						# str
		self.language = None					# str, according to ISO 639-1 (http://en.wikipedia.org/wiki/ISO_639-1)
		self.version = None						# str
		self.ucspec = None	                    # UCSpec
		self.actors = []						# Actor
		self.business_objects = []				# BusinessObject
		self.business_rules = []				# BusinessRule

class UCSpec(object):
	def __init__(self):
		self.priorities = []					# Priority
		self.goal_levels = []					# GoalLevel
		self.usecases = []						# UseCase

class UseCase(Referencable):
	def __init__(self, title):
		Referencable.__init__(self)

		self.title = title						# list of Items
		self.identifier = None					# str
		self.main_actors = []					# Actor
		self.other_actors = []					# Actor
		self.goal_level = None					# GoalLevel
		self.priority = None					# Priority

		self.triggers = []						# Trigger
		self.preconditions = []					# PreCondition
		self.postconditions = []				# PostCondition

		self.scenario = None					# Scenario

		self.testcases = []						# TestCases

class Scenario(object):
	def __init__(self):
		self.items = []							# list of Step, Scenario

class Step(Referencable):
	def __init__(self, items = []):
		Referencable.__init__(self)
		self.nextSteps = []                     # next possible Steps
		self.items = items						# list of Items
		self.scenario = None					# Scenario
		self.events = []						# list of Event

	def __str__(self):
		return " ".join([str(i) for i in self.items])

class Actor(Referencable):
	def __init__(self, name, identifier = None, description = None):
		Referencable.__init__(self)

		self.name = name						# str
		self.identifier = identifier			# str
		self.description = description			# str
		self.properties = None					# dict

class Item(object):
	def __init__(self):
		pass

class Reference(Item):
	def __init__(self, item, properties = None):
		item.referenced = True					# boolean
		self.item = item						# object that is Referencable
		self.properties = properties			# dict

class TextItem(Item):
	def __init__(self, text):
		self.text = text						# str

	def __str__(self):
		return self.text

class Command(Item):
	pass

class GoToCommand(Command):
	def __init__(self, step):
		step.referenced = True					# boolean
		self.step = step						# Step

	def __str__(self):
		return "GoTo %s" % str(id(self.step))

class EoUCCommand(Command):
	def __str__(self):
		return "EoUC"

class ScreenCommand(Command):
	def __init__(self, identifier):
		self.referenced = True					# boolean
		self.identifier = identifier			# str

##############
# Business
######

class BusinessObject(Referencable):
	def __init__(self, identifier, title):
		Referencable.__init__(self)

		self.title = title						# list of Items
		self.identifier = identifier
		self.description = None					# str
		#self.business_rules = []				# ?
		#self.properties = None					# ?

class BusinessRule(Referencable):
	def __init__(self, identifier):
		self.identifier = identifier			# str
		self.description = []					# list of Item

##############
# Trigger/Conditino
######

class Condition(object):
	def __init__(self, condition_type, items = []):
		self.condition_type = condition_type
		self.items = items						# list of items

class Trigger(Condition):
	def __init__(self, items):
		Condition.__init__(self, "trigger", items)

class PreCondition(Condition):
	def __init__(self, items):
		Condition.__init__(self, "pre-condition", items)

class PostCondition(Condition):
	def __init__(self, items):
		Condition.__init__(self, "post-condition", items)

##############
# Event
######

class EventType(object):
	ALTERNATION = "Alternation"
	EXTENSION   = "Extension"
	EXCEPTION   = "Exception"

class Event(object):
	def __init__(self, event_type, title):
		self.event_type = event_type			# EventType
		self.title = title						# list similar to items
		self.scenario = None					# Scenario

class AlternationEvent(Event):
	def __init__(self, title):
		Event.__init__(self, EventType.ALTERNATION, title)

class ExtensionEvent(Event):
	def __init__(self, title):
		Event.__init__(self, EventType.EXTENSION, title)

class ExceptionEvent(Event):
	def __init__(self, title):
		Event.__init__(self, EventType.EXCEPTION, title)

##############
# TestCases
######

class TestCases(object):
	def __init__(self):							# list of TestCase
		self.tests = []

	def __len__(self):
		return len(self.tests)

class TestCase(object):
	def __init__(self, path):
		self.path = path						# list of Step

	def __len__(self):
		return len(self.path)