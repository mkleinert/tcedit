<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1366304150724" ID="ID_102975994" MODIFIED="1366305287914" TEXT="[start]">
<attribute NAME="class" VALUE="start"/>
<node CREATED="1366304173280" ID="ID_1486890832" MODIFIED="1366305698585" POSITION="right" TEXT="[actor]">
<attribute NAME="class" VALUE="actor"/>
<node CREATED="1365075806601" FOLDED="true" ID="ID_1388770261" MODIFIED="1366871788996" TEXT="clicks">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365075927091" ID="ID_1106509568" MODIFIED="1366305358083" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365075931919" ID="ID_408806324" MODIFIED="1366305385094" TEXT="button">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="button"/>
</node>
</node>
<node CREATED="1365075940518" ID="ID_1555048693" MODIFIED="1366305407926" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365075952549" ID="ID_1240572311" MODIFIED="1366305428333" TEXT="link">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="link"/>
</node>
</node>
<node CREATED="1365076713135" ID="ID_247637728" MODIFIED="1366305412831" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076716453" ID="ID_1571316050" MODIFIED="1366305655144" TEXT="radio">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="radio button"/>
<node CREATED="1366305572219" ID="ID_1884529687" MODIFIED="1366305611703" TEXT="button">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="radio button"/>
</node>
</node>
</node>
</node>
<node CREATED="1365076228289" FOLDED="true" ID="ID_590008128" MODIFIED="1366871790501" TEXT="sets">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365076232583" ID="ID_1711156066" MODIFIED="1366305721988" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076236452" ID="ID_1128424833" MODIFIED="1366305733404" TEXT="window">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="window"/>
</node>
</node>
</node>
<node CREATED="1365076354532" FOLDED="true" ID="ID_1146403289" MODIFIED="1366871789986" TEXT="enters">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365076361597" ID="ID_1904747208" MODIFIED="1366305760239" TEXT="[value]">
<attribute NAME="class" VALUE="value"/>
<node CREATED="1365076365538" ID="ID_82378076" MODIFIED="1366305769901" TEXT="in">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1365076368026" ID="ID_1800471833" MODIFIED="1366305775555" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076375683" ID="ID_1662565849" MODIFIED="1366305796298" TEXT="text">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="text field"/>
<node CREATED="1366305780689" ID="ID_1691922155" MODIFIED="1366305813128" TEXT="field">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="text field"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1366306004498" FOLDED="true" ID="ID_790843911" MODIFIED="1366871791246" TEXT="returns">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1366306009123" ID="ID_137086896" MODIFIED="1366306037746" TEXT="to">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306011470" ID="ID_856868309" MODIFIED="1366306089017" TEXT="step">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="step"/>
<node CREATED="1366306012916" ID="ID_1314812857" MODIFIED="1366306053610" TEXT="[number]">
<attribute NAME="class" VALUE="number"/>
<node CREATED="1366306017494" ID="ID_890670208" MODIFIED="1366306059970" TEXT="and">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306020133" ID="ID_277612648" MODIFIED="1366306069349" TEXT="repeat">
<attribute NAME="class" VALUE="text"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1365076739409" FOLDED="true" ID="ID_191982934" MODIFIED="1366871791916" TEXT="uncheks">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365076744193" ID="ID_509416407" MODIFIED="1366306130856" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076746963" ID="ID_1983092670" MODIFIED="1366306143652" TEXT="checkbox">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="checkbox"/>
</node>
</node>
</node>
<node CREATED="1366306236440" FOLDED="true" ID="ID_1378016349" MODIFIED="1366871196686" TEXT="presses">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1366306240988" ID="ID_846650513" MODIFIED="1366306328244" TEXT="the">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306242527" ID="ID_124595774" MODIFIED="1366306333660" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366306245941" ID="ID_1219745070" MODIFIED="1366306347489" TEXT="button">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="button"/>
</node>
</node>
<node CREATED="1366306250460" ID="ID_1070055957" MODIFIED="1366306353012" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366306254023" ID="ID_1757394885" MODIFIED="1366306362841" TEXT="link">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="link"/>
</node>
</node>
<node CREATED="1366306256819" ID="ID_1862702496" MODIFIED="1366306388971" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366306260009" ID="ID_1304631662" MODIFIED="1366306400814" TEXT="radio">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="radio button"/>
<node CREATED="1366306263813" ID="ID_752040069" MODIFIED="1366306411695" TEXT="button">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="radio button"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1365076241013" FOLDED="true" ID="ID_139077284" MODIFIED="1366404526322" TEXT="selects">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365076296532" ID="ID_552195530" MODIFIED="1366306603133" TEXT="item">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="item"/>
<node CREATED="1365076306199" ID="ID_747167898" MODIFIED="1366306608193" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
</node>
<node CREATED="1365076313924" ID="ID_1193167282" MODIFIED="1366306614017" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366306576057" ID="ID_1445759804" MODIFIED="1366306618978" TEXT="from">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306577429" ID="ID_1222237711" MODIFIED="1366306626536" TEXT="the">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306578471" ID="ID_993086491" MODIFIED="1366306709442" TEXT="list">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306580562" ID="ID_266923809" MODIFIED="1366306641132" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1365076678893" FOLDED="true" ID="ID_802273945" MODIFIED="1366404546159" TEXT="checks">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365076691641" ID="ID_678015160" MODIFIED="1366306428556" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076695771" ID="ID_1844792563" MODIFIED="1366306441256" TEXT="checkbox">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="checkbox"/>
</node>
</node>
</node>
<node CREATED="1366404577136" FOLDED="true" ID="ID_1769795869" MODIFIED="1366404627264" TEXT="switches">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1366404582292" ID="ID_784318246" MODIFIED="1366404609519" TEXT="to">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366404583700" ID="ID_829318629" MODIFIED="1366404615433" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366404587189" ID="ID_707510473" MODIFIED="1366404625614" TEXT="screen">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="screen"/>
</node>
</node>
</node>
</node>
<node CREATED="1366306460271" FOLDED="true" ID="ID_1323830584" MODIFIED="1366404543649" TEXT="verifies">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1366306466385" ID="ID_459363018" MODIFIED="1366306500714" TEXT="that">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306467961" ID="ID_1556643418" MODIFIED="1366306505170" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366306471201" ID="ID_908167533" MODIFIED="1366306517542" TEXT="page">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="page"/>
<node CREATED="1366306473842" ID="ID_677007230" MODIFIED="1366306536471" TEXT="displays">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306477770" ID="ID_935520781" MODIFIED="1366306543894" TEXT="[value]">
<attribute NAME="class" VALUE="value"/>
<node CREATED="1366306480708" ID="ID_1042955394" MODIFIED="1366306552887" TEXT="message">
<attribute NAME="class" VALUE="text"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1365075983275" FOLDED="true" ID="ID_283009473" MODIFIED="1366404555632" TEXT="opens">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1365076010608" ID="ID_1333037594" MODIFIED="1366306781300" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076016411" ID="ID_1104970805" MODIFIED="1366306790701" TEXT="screen">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="screen"/>
</node>
</node>
<node CREATED="1365076151459" ID="ID_750221976" MODIFIED="1366306801589" TEXT="URL">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="url"/>
<node CREATED="1365076164348" ID="ID_555045199" MODIFIED="1366306809929" TEXT="[url]">
<attribute NAME="class" VALUE="url"/>
</node>
</node>
</node>
<node CREATED="1366306814617" FOLDED="true" ID="ID_1417919144" MODIFIED="1366404558297" TEXT="navigates">
<attribute NAME="action" VALUE=""/>
<attribute NAME="class" VALUE="action"/>
<node CREATED="1366306819122" ID="ID_31484765" MODIFIED="1366306851326" TEXT="to">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306820897" ID="ID_1739352607" MODIFIED="1366306856542" TEXT="the">
<attribute NAME="class" VALUE="text"/>
<node CREATED="1366306822231" ID="ID_1971782780" MODIFIED="1366306864202" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1366306825520" ID="ID_940663319" MODIFIED="1366306871981" TEXT="page">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="page"/>
</node>
</node>
<node CREATED="1366306829119" ID="ID_1245818337" MODIFIED="1366306880973" TEXT="URL">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="url"/>
<node CREATED="1366306831225" ID="ID_489132793" MODIFIED="1366306886621" TEXT="[url]">
<attribute NAME="class" VALUE="url"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1365076447684" FOLDED="true" ID="ID_470456392" MODIFIED="1366404562473" TEXT="displays">
<attribute NAME="class" VALUE="action"/>
<attribute NAME="action" VALUE=""/>
<node CREATED="1365076451460" ID="ID_1825663846" MODIFIED="1366305866899" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076457722" ID="ID_934277328" MODIFIED="1366305875979" TEXT="screen">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="screen"/>
</node>
</node>
<node CREATED="1365076460824" ID="ID_669000099" MODIFIED="1366305881507" TEXT="[name]">
<attribute NAME="class" VALUE="name"/>
<node CREATED="1365076468152" ID="ID_1944060081" MODIFIED="1366305894062" TEXT="menu">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="menu"/>
</node>
</node>
<node CREATED="1365076484635" ID="ID_1185870716" MODIFIED="1366305910087" TEXT="page">
<attribute NAME="class" VALUE="object"/>
<attribute NAME="type" VALUE="page"/>
<node CREATED="1365076487953" ID="ID_755292648" MODIFIED="1366305917997" TEXT="[url]">
<attribute NAME="class" VALUE="url"/>
</node>
</node>
</node>
</node>
</node>
</map>
